#pragma once

#ifdef __cplusplus 
extern "C" {
#endif

int drawBBoxOnVideo(const char *inputVideo, const char *bboxString, int bboxStrLen, const char *outputVideo);

#ifdef __cplusplus
}
#endif