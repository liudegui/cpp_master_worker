#include <iostream>
#include "zmq_messenger.h"
#include "messagebus.h"
#include "mockmessagehandler.h"
#include "loghelper.h"

using namespace RockLog;

static std::string generateWorkerId(int index)
{
    if (index > 99)
        return "";
    char id[3] = { 0 };
    sprintf(id, "%02d", index);
    std::string workerId = "ID" + std::string(id);
    return workerId;     // 返回workerId长度必须是4
}

int main(int argc, char *argv[])
{
    try
    {
        MockMessageHandler msgHandler;
        g_messagebus.attach<const char *, const char *, uint32_t>(kZmqRecvMSG, &MockMessageHandler::onRecvMsg, msgHandler);
        std::string workerId = generateWorkerId(0);
        subcribeMsg(workerId);

        while (true)    
        {
            usleep(100000); // sleep 0.1s
        }
    }
    catch (std::exception &e)
    {
        LOG(kErr) << "Exception: " << e.what() << "\n";
    }
    
    return 0;
}
