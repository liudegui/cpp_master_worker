#pragma once

#include <string>
#include <stdint.h>
#include "app_common.h"
#include "bytes_buffer.hpp"
class MockMessageHandler
{
public:
    MockMessageHandler() = default;
    ~MockMessageHandler() {}
    void onRecvMsg(const char * workerId, const char *msg, uint32_t len);

private:
    void doWorkerUploadVideoAndTracks(BytesBuffer &buf);
};
