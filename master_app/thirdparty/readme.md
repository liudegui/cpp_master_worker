## **[cpp_master_worker](https://gitee.com/liudegui/cpp_master_worker)**依赖第三方库地址

+ [loghelper](https://gitee.com/liudegui/loghelper)  : 这是我一直在使用的基于boost.log的日志库；
+ [concurrentqueue](https://github.com/cameron314/concurrentqueue.git) : A fast multi-producer, multi-consumer lock-free concurrent queue for C++11
+ [libzmq](https://github.com/zeromq/libzmq) :  ZeroMQ core engine in C++, implements ZMTP/3.1 和  [cppzmq](https://github.com/zeromq/cppzmq)  : Header-only C++ binding for libzmq



这是三个开源工程代码已上传至CSDN，可以从https://download.csdn.net/download/stallion5632/86247799打包下载

