#! /bin/bash
if [ ! -d bin ]; then
    mkdir bin
fi
if [ ! -d lib ]; then
    mkdir lib
fi
############
if [ -d thirdparty/loghelper ]; then
    cd thirdparty/loghelper
    chmod +x compile.sh
    ./compile.sh
    cd ../../
fi
cp thirdparty/loghelper/lib/*.so lib
cp thirdparty/loghelper/lib/*.so bin
#############
if [ ! -d build ]; then
    mkdir build
fi
cd build
cmake -DCMAKE_BUILD_TYPE:STRING=Release -DMASTER_DEBUG=$1 ..
make -j4
make install
cd ../

