#pragma once

#include <string>
#include <stdint.h>
#include "app_common.h"
#include "bytes_buffer.hpp"
class MessageHandler
{
public:
    static MessageHandler& instance()
    {
        static MessageHandler ins;
        return ins;
    }

    void init(AppConfig_t &cfg);
    ~MessageHandler() = default;

    void sendMsg(const std::string &workerId, const CameraInfo_t &info);
    void sendMsg(const std::string &workerId, const std::string &str);
    void sendMsg(const std::string &workerId, const char *msg, uint32_t len);
    void onRecvMsg(const char * workerId, const char *msg, uint32_t len);

private:
    MessageHandler() = default;
    void doWorkerUploadInfo(BytesBuffer &buf);
    void doWorkerUploadVideoAndTracks(BytesBuffer &buf);

    AppConfig_t m_cfg;
};
