﻿#pragma once
#include <string>
#include <vector>
#include <tuple>

// 上传单个文件
int httpUpload(const std::string &host, const std::string &port, const std::string &url,
         const std::string &filename, const char *fileData, uint64_t fileDataSize);
// 上传单个文件
int httpUpload(const std::string &host, const std::string &port, const std::string &url,
         const std::string &filename, const std::string &fileData);

// 同时上传文件名数组和字符串(如json流),字符串流可为空
int httpUpload(const std::string &host, const std::string &port, const std::string &url,
    const std::vector<std::string> &filenames, const std::string &stream = "");
// 同时上传文件内容数组和字符串(如json流),字符串流可为空
int httpUpload(const std::string &host, const std::string &port, const std::string &url,
         const std::vector<std::tuple<std::string, std::string>> &files, const std::string &stream = "");
// 普通httppost
int httpPost(const std::string &host, const std::string &port, const std::string &url, const std::string &data);
