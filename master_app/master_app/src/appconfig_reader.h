#pragma once
#include <stdint.h>
#include <string>
#include "app_common.h"


AppConfig_t loadAppConfig(const std::string &filepath);
