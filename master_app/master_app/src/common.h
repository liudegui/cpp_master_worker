#pragma once
#include <stdint.h>
#include <string>
#include "app_common.h"

static std::string generateWorkerId(int index)
{
    if (index > 99)
        return "";
    char id[3] = { 0 };
    sprintf(id, "%02d", index);
    std::string workerId = "ID" + std::string(id);
    return workerId; // 返回workerId长度必须是4
}