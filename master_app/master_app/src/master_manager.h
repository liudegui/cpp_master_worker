#pragma once
#include <vector>
#include "appconfig_reader.h"
#include "time.h"

class MessageHandler;
class MasterManager
{
public:
    MasterManager(AppConfig_t &cfg);
    ~MasterManager();
    void init();
    void loadInputConfig();

private:
    MasterManager() = delete;
    void startCheckInputConfigStatus();
    void checkInputConfigStatus();

    AppConfig_t m_cfg;
    time_t m_inputLastWriteTime = 0;        // last modify time of input.cfg
};
