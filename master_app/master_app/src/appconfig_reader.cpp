#include "appconfig_reader.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>

using namespace boost;

static const char *kAppConfigRoot = "daemon";
static const char *kProcessName = "ProcessName";
static const char *kInputConfig = "InputConfig";
static const char *kProcessNum = "ProcessNum";
static const char *kCheckProcessInterval = "CheckProcessInterval";
static const char *kSharedMemoryCheck = "SharedMemoryCheck";
static const char *kUploadServerIP = "UploadServerIP";
static const char *kUploadServerPort = "UploadServerPort";
static const char *kUploadServerURL = "UploadServerURL";

// load app configure
AppConfig_t loadAppConfig(const std::string &filepath)
{
    property_tree::ptree pt;
    property_tree::ini_parser::read_ini(filepath, pt);
    property_tree::ptree daemon;
    daemon = pt.get_child(kAppConfigRoot);

    AppConfig_t cfg;
    auto a = daemon.get_optional<std::string>(kProcessName);
    if (a)
        cfg.processName = *a;

    auto b = daemon.get_optional<uint32_t>(kCheckProcessInterval);
    if (b)
        cfg.checkProcessInterval = *b;

    auto c = daemon.get_optional<bool>(kSharedMemoryCheck);
    if (c)
        cfg.sharedMemoryCheck = *c;

    auto f = daemon.get_optional<std::string>(kInputConfig);
    if (f)
        cfg.inputConfigName = *f;

    auto serverIp = daemon.get_optional<std::string>(kUploadServerIP);
    if (serverIp)
        cfg.uploadServerIP = *serverIp;

    auto serverPort = daemon.get_optional<std::string>(kUploadServerPort);
    if (serverPort)
        cfg.uploadServerPort = *serverPort;
    
    auto serverUrl = daemon.get_optional<std::string>(kUploadServerURL);
    if (serverUrl)
        cfg.uploadServerURL = *serverUrl;
    
    auto processNum = daemon.get_optional<uint32_t>(kProcessNum);
    if (processNum)
        cfg.processNum = *processNum;
    
    std::cout << "cfg.processNum: " << cfg.processNum << std::endl;
    return cfg;
}
