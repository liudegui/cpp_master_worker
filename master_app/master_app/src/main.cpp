#include <iostream>
#include <limits>
#include <boost/filesystem.hpp>
#include "appconfig_reader.h"
#include "processmanager.h"
#include "master_manager.h"
#include "loghelper.h"
#include "utility.hpp"
using namespace RockLog;

int main(int argc, char *argv[])
{
    try
    {
        LOG(kInfo) << "MASTER_APP"
                   << "Compile Date:"
                   << "__DATE__" << __DATE__
                   << "__TIME__" << __TIME__;   //compile data
        // 加载程序配置文件
        AppConfig_t cfg;
        const std::string cfgFilePath = boost::filesystem::current_path().string() + std::string("/master_app.cfg");
        if (boost::filesystem::exists(cfgFilePath))
            cfg = std::move(loadAppConfig(cfgFilePath));

        // 启动子进程和监控定时器
        ProcessManager pm(cfg);
        pm.init();
        pm.startWorkerProcess();
        
        // 启动配置下发，数据接受等业务
        MasterManager mm(cfg);
        mm.init();
        Utility::sleep(3);
        mm.loadInputConfig();

        while (true)
        {
            Utility::usleep(100000); // sleep 0.1s
        }
    }
    catch (std::exception &e)
    {
        LOG(kErr) << "Exception: " << e.what() << "\n";
    }
    
    return 0;
}
