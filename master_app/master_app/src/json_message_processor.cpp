#include <exception>
#include <iostream>
#include <boost/property_tree/json_parser.hpp>
#include "utility.hpp"
#include "json_message_processor.h"
#include "loghelper.h"

using namespace boost;
using namespace boost::property_tree;
using namespace std;
using namespace RockLog;

//////////////////////////////////////////////////////////////////////

bool JsonMessageProcessor::decodeCameraInfo(const std::string& filename, std::vector<CameraInfo_t>& rsp)
{
    try
    {
        read_json(filename, m_pt);
        auto camera = m_pt.get_child_optional("camera");
        if (camera)
        {
            for (auto &item : *camera)
            {
                CameraInfo_t t;
                auto name = item.second.get_optional<std::string>("name");
                if (name)
                    std::copy((char*)name->data(), (char*)name->data() + name->length(), t.name);

                auto main_stream = item.second.get_optional<std::string>("main_stream");
                if (main_stream)
                    std::copy((char*)main_stream->data(), (char*)main_stream->data() + main_stream->length(), t.main_stream);

                auto alg_param_file = item.second.get_optional<std::string>("alg_param_file");
                if (alg_param_file)
                    std::copy((char*)alg_param_file->data(), (char*)alg_param_file->data() + alg_param_file->length(), t.alg_param_file);
                
                auto camera_id = item.second.get_optional<std::string>("camera_id");
                if (camera_id)
                    std::copy((char*)camera_id->data(), (char*)camera_id->data() + camera_id->length(), t.camera_id);

                auto main_pixel_w = item.second.get_optional<uint32_t>("main_pixel_w");
                if (main_pixel_w)
                    t.main_pixel_w = *main_pixel_w;

                auto main_pixel_h = item.second.get_optional<uint32_t>("main_pixel_h");
                if (main_pixel_h)
                    t.main_pixel_h = *main_pixel_h;
                
                auto main_frame_rate = item.second.get_optional<uint32_t>("main_frame_rate");
                if (main_frame_rate)
                    t.main_frame_rate = *main_frame_rate;

                rsp.push_back(t);
                LOG(kInfo) << "name:" << t.name << ",main_pixel_w:" << t.main_pixel_w;
            }
        }

        return true;
    }
    catch (std::exception &e)
    {
        LOG(kErr) << "[E] decodeCameraInfo failed, error:" << e.what();
    }
    return false;
}



