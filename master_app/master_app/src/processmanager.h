#pragma once
#include <vector>
#include "appconfig_reader.h"
#include "shared_memory_checker.hpp"

class ProcessManager
{
public:
    ProcessManager(AppConfig_t &cfg);
    ~ProcessManager();
    void init();
    void startCheckTimer();
    bool startWorkerProcess();

private:
    ProcessManager() = delete;
    void checkProcessStatus();

    std::vector<uint32_t> m_pids;           // all sub-process pid list
    AppConfig_t m_cfg;
    std::vector<SharedMemoryChecker> m_shmCheckers;
    time_t m_inputLastWriteTime = 0;        // last modify time of input.cfg
};
