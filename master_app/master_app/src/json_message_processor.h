#pragma once
#include <string>
#include <boost/property_tree/ptree.hpp>
#include "app_common.h"


// json message manager
class JsonMessageProcessor
{
public:
    JsonMessageProcessor() = default;
    ~JsonMessageProcessor() = default;

    bool decodeCameraInfo(const std::string& filename, std::vector<CameraInfo_t>& rsp);
    

private:
    boost::property_tree::ptree m_pt;
};

