## 工程说明

推理引擎服务master-worker机制的master部分

master与worker分工如下：

engine-master:
+ 关联工程: engine-worker；
+ 检测结果视频和轨迹http上传到http服务器；
+ 进程管理，管理多个worker进程；
+ 进程检测，检测worker进程是否存活，不存活杀死重启；
+ 任务下发，下发任务至worker；
+ 转发worker上报的检测结果。

engine-worker:
+ 接收master下发的任务和配置；
+ 解码视频帧送入解码队列；
+ 从解码队列读取视频帧，并送入检测模块检测;
+ 将检测事件视频和轨迹发送给master进程；


程序执行：
+ 执行: ./master_app

## 目录结构
+ message_handler：zmq消息收发和消息内容处理，http上报信息；
+ zmq_messenger：zmq实例封装；
+ test_draw_app：测试程序；订阅轨迹和视频，本地叠加轨迹和视频，以验证效果；
+ master_app：应用入口程序；读取配置文件，管理启停worker进程，监控worker进程心跳。

## clone代码
代码中依赖子模块loghelper，下载代码方式如下：

+ git submodule update
+ git submodule sync