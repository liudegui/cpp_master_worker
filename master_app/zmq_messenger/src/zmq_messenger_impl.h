#pragma once

#include <stdint.h>
#include <zmq.hpp>
#include <string>

class ZMQMessengerImpl
{
public:

    static ZMQMessengerImpl& instance()
    {
        static ZMQMessengerImpl ins;
        return ins;
    }

    void subcribeMsg(const std::string &workerId);
    void publishMsg(const std::string &workerId, const std::string& msg);
    void publishMsg(const std::string &workerId, const char* msg, uint32_t len);

private:
    ZMQMessengerImpl();
    zmq::context_t m_context;
    zmq::socket_t m_publisher;
};