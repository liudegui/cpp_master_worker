#include "zmq_messenger.h"
#include "zmq_messenger_impl.h"
#include <iostream>


void subcribeMsg(const std::string &workerId)
{
    ZMQMessengerImpl::instance().subcribeMsg(workerId);
}

void publishMsg2(const std::string &workerId, const std::string& msg)
{
    ZMQMessengerImpl::instance().publishMsg(workerId, msg);
}

void publishMsg(const std::string &workerId, const char* msg, uint32_t len)
{
    ZMQMessengerImpl::instance().publishMsg(workerId, msg, len);
}
