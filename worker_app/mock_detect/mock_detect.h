#pragma once

#include <opencv2/core.hpp>
#include "mock_detect_def.h"

int initDetect(const std::string& haarcascadePath);
int detect(cv::Mat frame, Rect_t &rect);
