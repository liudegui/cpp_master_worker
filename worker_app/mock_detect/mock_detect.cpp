#include <opencv2/objdetect.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>

#include "mock_detect.h"

using namespace std;
using namespace cv;

/** Global variables */
CascadeClassifier face_cascade;

int initDetect(const std::string &haarcascadePath)
{
    //-- 1. Load the cascades
    if (!face_cascade.load(haarcascadePath))
    {
        cout << "--(!)Error loading face cascade\n";
        return -1;
    };

    return 0;
}

int detect(cv::Mat frame, Rect_t &rect)
{
    cv::Mat frame_gray;
    cvtColor(frame, frame_gray, COLOR_BGR2GRAY);
    equalizeHist(frame_gray, frame_gray);

    //-- Detect faces
    std::vector<Rect> faces;
    face_cascade.detectMultiScale(frame_gray, faces);
    if (faces.size() > 0)   // 为了和实际情况类似，只取第一个人脸
    {
        rect.x = faces[0].x;
        rect.y = faces[0].y;
        rect.h = faces[0].height;
        rect.w = faces[0].width;
    }
    return faces.size();
}