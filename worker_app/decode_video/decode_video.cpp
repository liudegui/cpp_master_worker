﻿// ref:https://github.com/FFmpeg/FFmpeg/blob/master/doc/examples/hw_decode.c
// ref: https://github.com/chinahbcq/ffmpeg_hw_decode
// ref: https://www.jianshu.com/p/3ea9ef713211

#include "decode_video.h"
#include <stdio.h>
#include <string>
#include <unistd.h>

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/hwcontext.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
#include <libavutil/opt.h>
}

static AVFormatContext *s_input_ctx  = NULL;
static AVCodecContext *s_decoder_ctx = NULL;
static int s_video_stream            = -1;
static bool s_is_init                = false;

static volatile int DISTURBE_SIGNALS = 0;
static int decode_interrupt_cb(void *ctx)
{
    return DISTURBE_SIGNALS > 0;
}
static const AVIOInterruptCB int_cb = {decode_interrupt_cb, NULL};

static AVBufferRef *s_hw_device_ctx = NULL;
static AVPixelFormat s_hw_pix_fmt;

static AVPixelFormat s_pixel_format = AV_PIX_FMT_BGR24; // AV_PIX_FMT_BGR24对应opencv中的CV_8UC3
static int s_thread_count           = 4;
static const char *s_hwdevice_name  = "cuda"; // 默认使用cuda
AVBitStreamFilterContext *s_mp4bsfc = NULL;

// decode_type 0-cpu 1-cuda 2-nvmpi
int init_ctx(const char *url, int decode_type, int deviceIndex = 0);
void clear_ctx();
static int hw_decoder_init(AVCodecContext *ctx, const AVHWDeviceType type, int deviceIndex = 0)
{
    int err = 0;

    if ((err = av_hwdevice_ctx_create(&s_hw_device_ctx, type,
                                      std::to_string(deviceIndex).c_str(), NULL, 0))
        < 0)
    {
        fprintf(stderr, "Failed to create specified HW device.\n");
        return err;
    }
    ctx->hw_device_ctx = av_buffer_ref(s_hw_device_ctx);

    return err;
}

static AVPixelFormat get_hw_format(AVCodecContext *ctx, const AVPixelFormat *pix_fmts)
{
    const AVPixelFormat *p;

    for (p = pix_fmts; *p != -1; p++)
    {
        if (*p == s_hw_pix_fmt)
            return *p;
    }

    fprintf(stderr, "Failed to get HW surface format.\n");
    return AV_PIX_FMT_NONE;
}
///////////////////////////////////////////
// decode_type 0-cpu 1-cuda 2-nvmpi
int init_ctx(const char *url, int decode_type, int deviceIndex)
{
    if (s_is_init)
        return 0;

    av_log_set_level(AV_LOG_INFO);

    int ret = -1;

    s_input_ctx = avformat_alloc_context();
    if (!s_input_ctx)
    {
        ret = AVERROR(ENOMEM);
        return -1;
    }
    // 设置input_ctx的初始化参数
    s_input_ctx->video_codec_id = AV_CODEC_ID_NONE;
    s_input_ctx->audio_codec_id = AV_CODEC_ID_NONE;
    s_input_ctx->flags |= AVFMT_FLAG_NONBLOCK;
    s_input_ctx->interrupt_callback = int_cb;

    AVHWDeviceType type;
    if (1 == decode_type)
    {
        type = av_hwdevice_find_type_by_name(s_hwdevice_name);
        if (type == AV_HWDEVICE_TYPE_NONE)
        {
            fprintf(stderr, "Device type %s is not supported.\n", s_hwdevice_name);
            fprintf(stderr, "Available device types:");
            while ((type = av_hwdevice_iterate_types(type)) != AV_HWDEVICE_TYPE_NONE)
                fprintf(stderr, " %s", av_hwdevice_get_type_name(type));
            fprintf(stderr, "\n");
            return -2;
        }
    }

    // open the input file
    if (avformat_open_input(&s_input_ctx, url, NULL, NULL) != 0)
    {
        fprintf(stderr, "Cannot open input file '%s'\n", url);
        return -3;
    }

    if (avformat_find_stream_info(s_input_ctx, NULL) < 0)
    {
        fprintf(stderr, "Cannot find input stream information.\n");
        return -4;
    }

    // find the video stream information
    AVCodec *decoder = NULL;
    ret              = av_find_best_stream(s_input_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, &decoder, 0);
    if (ret < 0)
    {
        fprintf(stderr, "Cannot find a video stream in the input file\n");
        return -5;
    }
    s_video_stream              = ret;
    AVCodecParameters *codecpar = s_input_ctx->streams[s_video_stream]->codecpar;
    int32_t codec_id            = codecpar->codec_id;

    if (1 == decode_type)
    {
        for (int i = 0;; i++)
        {
            const AVCodecHWConfig *config = avcodec_get_hw_config(decoder, i);
            if (!config)
            {
                fprintf(stderr, "Decoder %s does not support device type %s.\n", decoder->name, av_hwdevice_get_type_name(type));
                return -6;
            }
            if (config->methods & AV_CODEC_HW_CONFIG_METHOD_HW_DEVICE_CTX && config->device_type == type)
            {
                s_hw_pix_fmt = config->pix_fmt;
                break;
            }
        }
    }
    else
    {
        if (2 == decode_type) // nvmpi
        {
            const char *name;
            if (codecpar->codec_id == AV_CODEC_ID_H264)
                name = "h264_nvmpi";
            else if (codecpar->codec_id == AV_CODEC_ID_H265)
                name = "hevc_nvmpi";
            else
                name = "unsupport";
            decoder = avcodec_find_decoder_by_name(name);
            if (decoder == NULL)
            {
                fprintf(stderr, "failed to load codec %s", name);
                return -7;
            }
        }
        else // cpu
        {
            decoder = avcodec_find_decoder(codecpar->codec_id);
            if (!decoder)
            {
                fprintf(stderr, "Codec not found\n");
                return -7;
            }
        }
    }

    // duration
    int64_t duration = s_input_ctx->duration / AV_TIME_BASE;
    // fps
    int fps_num = s_input_ctx->streams[s_video_stream]->r_frame_rate.num;
    int fps_den = s_input_ctx->streams[s_video_stream]->r_frame_rate.den;
    double fps  = 0.0;
    if (fps_den > 0)
    {
        fps = fps_num / fps_den;
    }
    av_log(NULL, AV_LOG_INFO, "duration:%ld,\nfps:%f,\n", duration, fps);

    s_decoder_ctx = avcodec_alloc_context3(decoder);
    if (!s_decoder_ctx)
    {
        fprintf(stderr, "Could not allocate video codec context\n");
        return -8;
    }

    if (1 == decode_type)
    {
        if (avcodec_parameters_to_context(s_decoder_ctx, codecpar) < 0)
            return -9;

        s_decoder_ctx->get_format = get_hw_format;
        // 硬解码不需要赋值,从硬件读取, AV_PIX_FMT_NV12;
        s_decoder_ctx->pix_fmt = AV_PIX_FMT_NV12;

        if (hw_decoder_init(s_decoder_ctx, type, deviceIndex) < 0)
        {
            fprintf(stderr, "hw decoder init failed\n");
            return -10;
        }
    }
    else
    {
        // 软编码需要赋值
        // FIX: FFmpeg deprecated pixel format used, ref:https://blog.csdn.net/xionglifei2014/article/details/90710797
        switch (s_decoder_ctx->pix_fmt)
        {
        case AV_PIX_FMT_YUVJ420P:
            s_decoder_ctx->pix_fmt = AV_PIX_FMT_YUV420P;
            break;
        case AV_PIX_FMT_YUVJ422P:
            s_decoder_ctx->pix_fmt = AV_PIX_FMT_YUV422P;
            break;
        case AV_PIX_FMT_YUVJ444P:
            s_decoder_ctx->pix_fmt = AV_PIX_FMT_YUV444P;
            break;
        case AV_PIX_FMT_YUVJ440P:
            s_decoder_ctx->pix_fmt = AV_PIX_FMT_YUV440P;
            break;
        default:
            s_decoder_ctx->pix_fmt = AVPixelFormat(codecpar->format);
        }

        s_decoder_ctx->height       = codecpar->height;
        s_decoder_ctx->width        = codecpar->width;
        s_decoder_ctx->thread_count = s_thread_count;  // 设置解码线程数目
        s_decoder_ctx->thread_type  = FF_THREAD_FRAME; // 设置解码type
    }

    av_opt_set_int(s_decoder_ctx, "refcounted_frames", 1, 0);

    if ((ret = avcodec_open2(s_decoder_ctx, decoder, NULL)) < 0)
    {
        fprintf(stderr, "Failed to open codec for stream #%u\n", s_video_stream);
        return -11;
    }

    if (codec_id == AV_CODEC_ID_H264)
    {
        s_mp4bsfc = av_bitstream_filter_init("h264_mp4toannexb");
    }
    else if (codec_id == AV_CODEC_ID_HEVC)
    {
        s_mp4bsfc = av_bitstream_filter_init("hevc_mp4toannexb");
    }
    else
    {
        printf("invalid video format, should be h264 or h265.");
        return -12;
    }

    av_log(NULL, AV_LOG_INFO, "codecpar->codec_id:%d,\ndecoder_ctx->codec_id:%d,\n", codecpar->codec_id, s_decoder_ctx->codec_id);
    av_log(NULL, AV_LOG_INFO, "codecpar->format:%d,\ndecoder_ctx->pix_fmt:%d,\nformat:%d,\n", codecpar->format, s_decoder_ctx->pix_fmt, s_pixel_format);

    s_is_init = true;
    return 0;
}

void clear_ctx()
{
    if (s_is_init)
    {
        avcodec_close(s_decoder_ctx);
        av_freep(&s_decoder_ctx);
        avformat_close_input(&s_input_ctx);
        s_video_stream = -1;
        s_is_init      = false;
    }
}

bool support_hwdevice()
{
    AVHWDeviceType type;
    type = av_hwdevice_find_type_by_name(s_hwdevice_name);
    if (type == AV_HWDEVICE_TYPE_NONE)
    {
        fprintf(stderr, "Device type %s is not supported.\n", s_hwdevice_name);
        fprintf(stderr, "Available device types:");
        while ((type = av_hwdevice_iterate_types(type)) != AV_HWDEVICE_TYPE_NONE)
            fprintf(stderr, " %s", av_hwdevice_get_type_name(type));
        fprintf(stderr, "\n");
        return false;
    }
    return true;
}

// decode_type 0-cpu 1-cuda 2-nvmpi
int ffmpeg_video_decode(const char *url, void (*callback)(AVFrame *frame_bgr, AVPacket *packet, int decode_type),
                        int decode_type, bool only_key_frame, bool &isExit, bool mp4file, int deviceIndex)
{
    av_log(NULL, AV_LOG_INFO, "stream path:%s, \ndecode_type:%d, \nonly_key_frame:%s,\n",
           url, decode_type, only_key_frame ? "true" : "false");

    int ret = init_ctx(url, decode_type, deviceIndex);
    if (0 != ret)
    {
        fprintf(stderr, "init failed!ret: %d\n", ret);
        return -1;
    }

    // 视频流中解码的frame
    AVFrame *frame    = NULL;
    AVFrame *sw_frame = NULL;
    AVPacket packet;
    ret                = -1;
    int err            = -1;
    AVFrame *tmp_frame = NULL;

    frame = av_frame_alloc();
    if (1 == decode_type)
        sw_frame = av_frame_alloc(); // transfer frame data from GPU to CPU

    av_log(NULL, AV_LOG_INFO, "width:%d,\nheight:%d,\n", s_decoder_ctx->width, s_decoder_ctx->height);

    // 开始解码
    while (!isExit && av_read_frame(s_input_ctx, &packet) >= 0)
    {
        if (packet.stream_index != s_video_stream || (only_key_frame && !(packet.flags & AV_PKT_FLAG_KEY)) || // 不是关键帧就会跳过
            packet.size < 1)
        {
            goto discard_packet;
        }

        if (mp4file && s_mp4bsfc)
        {
            usleep(40 * 1000);
            AVPacket new_pkt = packet;
            ret              = av_bitstream_filter_filter(s_mp4bsfc, s_input_ctx->streams[s_video_stream]->codec,
                                             NULL, &new_pkt.data, &new_pkt.size, packet.data, packet.size, (packet.flags & AV_PKT_FLAG_KEY));
            if (ret < 0)
            {
                printf("av_bitstream_filter_filter failed!");
            }
            else
            {
                if (new_pkt.data != packet.data)
                {
                    av_free_packet(&packet);
                    packet.data = new_pkt.data;
                    packet.size = new_pkt.size;
                }
            }
        }

        // 解码packet
        err = avcodec_send_packet(s_decoder_ctx, &packet);
        if (err != AVERROR(EAGAIN) && err != AVERROR_EOF && err < 0)
        {
            fprintf(stderr, "Error during decoding\n");
            goto discard_packet;
        }
        err = avcodec_receive_frame(s_decoder_ctx, frame);
        if (err == AVERROR(EAGAIN))
        {
            goto discard_packet;
        }
        else if (err == AVERROR_EOF)
        {
            av_packet_unref(&packet);
            break;
        }
        else if (err < 0)
        {
            fprintf(stderr, "Error while decoding\n");
            goto discard_packet;
        }

        if (1 == decode_type)
        {
            if (frame->format == s_hw_pix_fmt)
            {
                // retrieve data from GPU to CPU
                if ((ret = av_hwframe_transfer_data(sw_frame, frame, 0)) < 0)
                {
                    fprintf(stderr, "Error transferring the data to system memory\n");
                    goto discard_packet;
                }
                tmp_frame = sw_frame;
            }
            else
                tmp_frame = frame;
        }
        else
        {
            tmp_frame = frame;
        }

        callback(tmp_frame, &packet, decode_type);

    discard_packet:
        av_frame_unref(frame);
        if (1 == decode_type)
            av_frame_unref(sw_frame);
        av_packet_unref(&packet);
    }

    if (s_mp4bsfc)
    {
        av_bitstream_filter_close(s_mp4bsfc);
    }
    // clear
    av_frame_unref(frame);
    av_freep(&frame);
    clear_ctx();

    return 0;
}
