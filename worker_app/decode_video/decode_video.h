#pragma once

extern "C"
{
#include <libavcodec/avcodec.h>
}
/*
 * @description                 : 用于查询是否支持硬件解码
*/

bool support_hwdevice();

/*
 * @description                 : 解码
 * @param(url)                  : 解码输入，rtsp/mp4 均可
 * @param(callback)             : 处理回调函数
 * @param(decode_type)          : 解码方式  0-cpu 1-cuda 2-nvmpi 
 * @param(only_key_frame)       : 是否只获取关键帧
 * @param(isExit)               : 是否停止解码
 * @param(mp4file)              : url是否为mp4文件
 * @param(deviceIndex)          : 若为cuda解码，用于指定显卡，默认为0号显卡
 * @return                      : 0->OK    other->NG
*/
int ffmpeg_video_decode(const char *url, void (*callback)(AVFrame *frame, AVPacket *packet, int decode_type),
                        int decode_type, bool only_key_frame, bool &isExit, bool mp4file, int deviceIndex = 0);
