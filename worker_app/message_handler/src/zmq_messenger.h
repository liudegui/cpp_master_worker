#pragma once

#include <stdint.h>
#include <zmq.hpp>
#include <string>

class ZMQMessenger
{
public:
    ZMQMessenger(const std::string& id);
    void publishMsg(const std::string& msg);
    void publishMsg(const char* msg, uint32_t len);
    void init();

private:
    ZMQMessenger();
    void subcribeMsg();

    zmq::context_t m_context;
    zmq::socket_t m_publisher;
    std::string m_workerId;
};