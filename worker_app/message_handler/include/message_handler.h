#pragma once
#include <string>
#include "bytes_buffer.hpp"

class ZMQMessenger;
class MessageHandler
{
public:
    static MessageHandler& instance()
    {
        static MessageHandler ins;
        return ins;
    }

    ~MessageHandler();
    void init(const std::string &workerId);
    bool publishVideoAndTracks(const std::string& tracksStr, const std::string& video_file);
    bool publishVideoAndTracks(const std::string& tracksStr, const std::string& videoName, const std::string& videoContent);
    void onRecvMsg(const char *msg, uint32_t len);
private:

    void doCameraInfo(BytesBuffer &buf);
    MessageHandler() = default;
    std::string  m_workerId;
    ZMQMessenger *m_messenger = 0;
    bool m_startHeartBeat = true;
};

