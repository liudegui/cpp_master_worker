#pragma once
#include <opencv2/core.hpp>
#include "blockingconcurrentqueue.h"
#include "mock_detect_def.h"
#include "app_common.h"

class DecodeDetectVideo
{
public: 
    DecodeDetectVideo();
    ~DecodeDetectVideo()= default;
    void init(const std::string &workerId);

private: 
    void startDetectFrame();
    void startVideoDecode();
    void start(CameraInfo_t t);
    void restart(CameraInfo_t t);
    std::string serializeTrajectoryInfo(const std::string &video_name);
    void publishVideoAndTracks();
private:
    std::string         m_workerId;
    CameraInfo_t        m_cameraInfo;    // 摄像头相关配置
    TrajectoryInfo_t     m_info;          // 上传轨迹的相关信息

    int m_decode_type  = 1;             // 解码类型 0-cpu 1-cuda 2-nvmpi
    bool m_only_key_frame = false;      // 是否只使用关键帧
    bool m_started = false;             // 解码和检测是否已经启动
};

