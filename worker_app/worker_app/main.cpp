
#include "decode_detect_video.h"
#include "message_handler.h"
#include "loghelper.h"
#include "utility.hpp"
#include "mock_detect.h"
#include "messagebus.h"

using namespace RockLog;

static std::string generateWorkerId(int index)
{
    if (index > 99)
        return "";
    char id[3] = { 0 };
    sprintf(id, "%02d", index);
    std::string workerId = "ID" + std::string(id);
    return workerId;     // 返回workerId长度必须是4
}

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        LOG(kInfo)<< "useage: ./app index(1-99) video_file";
        return -1;
    }
    std::string workerId = generateWorkerId(std::atoi(argv[1]));
    MessageHandler::instance().init(workerId);

    DecodeDetectVideo worker;
    worker.init(workerId);

#ifdef WORKER_DEBUG
    /////// for test /////////////////
    CameraInfo_t t;
    strcpy(t.alg_param_file, "haarcascade_frontalface_alt.xml");
    strcpy(t.camera_id, "123456");
    strcpy(t.main_stream, "/home/dgliu/vipc_linux_x64/face_1280_720.h264");
    strcpy(t.name, "camera_test");
    t.main_pixel_w = 1280;
    t.main_pixel_h = 720;
    t.main_frame_rate = 25;
    g_messagebus.sendMessage(kRecvCameraInfo, t);
    /////// for test /////////////////
#endif

    while (true)
    {
        sleep(1);
    }

    return 0;
}
