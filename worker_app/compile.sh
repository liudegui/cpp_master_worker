#! /bin/bash
if [ ! -d build ]; then
    mkdir build
fi
cd build
cmake -DWORKER_DEBUG=$1 ..
make -j4
make install
cd ../

