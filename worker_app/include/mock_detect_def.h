#pragma once

#include <stdint.h>
#include <vector>
#include <string>

struct Rect_t
{
    uint32_t x   = 0;       //
    uint32_t y   = 0;       //
    uint32_t w   = 0;       // 宽
    uint32_t h   = 0;       // 高
};

struct Track_t
{
    uint32_t f   = 0;       // 帧序号
    Rect_t  rect;           //
};

// 上报轨迹信息
struct TrajectoryInfo_t
{
    std::string             video_name;
    uint32_t                video_width;
    uint32_t                video_height;
    uint32_t                video_framerate;
    std::vector<Track_t>    tracks;
};