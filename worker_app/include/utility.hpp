#pragma once
#include <chrono>
#include <stdint.h>

// #include <iostream>
#include <sstream>
#include <iomanip>
#include <future>

namespace Utility
{
    static uint64_t getTimeStamp()
    {
        std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::system_clock::now().time_since_epoch());

        return ms.count();
    }

    static std::string getTimeString()
    {
        auto t = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        std::stringstream ss;
        ss << std::put_time(std::localtime(&t), "%Y-%m-%d-%H-%M-%S");
        return ss.str();
    }

    static void sleep(uint32_t n)
    {
        std::this_thread::sleep_for(std::chrono::seconds(n));
    }

    static void usleep(uint32_t n)
    {
        std::this_thread::sleep_for(std::chrono::microseconds(n));
    }
}