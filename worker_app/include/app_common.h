#pragma once

#include <stdint.h>
#include <string>


static const char* kZmqRecvMSG                        = "ZmqRecvMSG";               // 消息总线分发使用
static const char* kRecvCameraInfo                    = "RecvCameraInfo";           // 收到摄像机信息
static const uint32_t kSharedMemorySize               = 128;                        // 128 bytes
static const uint32_t kWorkIdLength                   = 4;                          // worker进程id的长度固定为4
static const char* kZMQServerURL                      = "ipc://zmqserver.ipc";      // 守护进程的zmq url
static const uint32_t kRecordVideoLenth               = 10;                         // 录制视频长度，单位秒

////////////////////////////////////////////
// 消息类型 master和worker需对应一致
static const uint32_t kJsonMessage                    = 1000;   // 收到的zmq消息是json类型，默认可按json类型decode
static const uint32_t kStringMessage                  = 1001;   // 收到的zmq消息是string类型，无需考虑消息截断问题
static const uint32_t kCameraInfo_t                   = 2000;   // 收到的zmq消息是CameraInfo_t结构体，按照该结构体解析
static const uint32_t kWorkerUploadVideoAndTracks_t   = 2001;   // 收到的zmq消息是WorkerUploadVideoAndTracks_t 结构体，按照该结构体解析
///////////////////测试使用的结构体////////////////////////
// 摄像机下发信息
struct CameraInfo_t
{
    char name[64]               = {0};     // 摄像机名称
    char main_stream[128]       = {0};     // 主流rtsp url
    char alg_param_file[128]    = {0};     // 算法库配置文件
    char camera_id[64]          = {0};     // id
    uint32_t main_pixel_w         = 0;     // width
    uint32_t main_pixel_h         = 0;     // hight
    uint32_t main_frame_rate      = 0;     // 帧率
};

// 需要上传给httpserver
struct VideoAndTracks_t
{
    std::string tracksStr;                  // 轨迹(未base64)
    std::string videoName;                  // 视频文件名
    std::string videoData;                  // 视频文件内容
};
///////////////////测试使用的结构体////////////////////////