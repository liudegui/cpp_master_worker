# 高空抛物解码和检测
## 工程说明

推理引擎服务master-worker机制的worker部分
master与worker分工如下：

engine-worker:
+ 关联工程: engine-master；
+ 接收master下发的任务和配置；
+ 解码视频帧送入解码队列；
+ 从解码队列读取视频帧，并送入检测模块检测;
+ 将检测事件视频和轨迹发送给master进程；

engine-master
+ 检测结果视频和轨迹http上传到http服务器；
+ 进程管理，管理多个worker进程；
+ 进程检测，检测worker进程是否存活，不存活杀死重启；
+ 任务下发，下发任务至worker；
+ 转发worker上报的检测结果。

注意：目前使用的是opencv官方示例中的人脸检测模型检测(haarcascade)。***待替换成高抛检测算法***

## **目录结构**

+ decode_video：视频解复用和解码模块；
+ message_handler：消息收发和消息内容处理模块
+ mock_detect：模拟的算法检测模块；
+ worker_app：入口程序，集成解码、检测程序；
+ mock_detect_test：模拟算法检测模块的测试程序
+ include目录为使用到的公共头文件；

## clone代码
代码中依赖子模块loghelper，下载代码方式如下：

+ git submodule update
+ git submodule sync

## 其它说明

+ decode_video:使用到ffmpeg，支持cpu解码、amd64平台的cuda解码 和 NX平台的Nvmpi解码；
+ zmq的接受(recv)是完全阻塞式的，为此创建了单独的线程；引用libzmq库的同时，还引用了zmq.hpp封装，具体请查看相关头文件；
+ message_handler和其它模块为了松耦合，使用“消息总线”关联，具体请查看`messagebus.hpp`，需要使用到boost::any;
+ 因为master和worker进程最终都放在一个文件夹部署，为防止.so组件混淆错乱，所有模块均编译成静态.a库；
+ 解码和推理线程间添加消息队列缓存，使用到开源的`concurrentqueue`，该组件内部支持线程同步；
+ 该worker进程可单独调试，编译的时候可添加`WORKER_DEBUG`宏；
+ loghelper为仓库的单独日志库，是boost.log的wapper；可输出到控制台、文件、syslog，支持C++的"<<"和C类型“printf”，具体请查看仓库代码。

## 调试

该工程添加宏`WORKER_DEBUG`支持单独调试，具体请查看worker_app的CMakeLists.txt。

## cmake系统环境变量支持
在CMakeLists.txt中添加
```
string(REPLACE ":" ";" OS_LIBRARY_DIRS $ENV{LD_LIBRARY_PATH})
link_directories( ${OS_LIBRARY_DIRS} )

string(REPLACE ":" ";" OS_CPLUS_INCLUDE_PATH $ENV{CPLUS_INCLUDE_PATH})
include_directories( ${OS_CPLUS_INCLUDE_PATH} )
```
