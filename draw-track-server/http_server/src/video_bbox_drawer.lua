
local ffi = require("ffi")

local _M = {
    _VERSION = '0.01'
}

ffi.cdef[[
    int drawBBoxOnVideo(const char *inputVideo, const char *bboxString, int bboxStrLen, const char *outputVideo);
]]

local lib = ffi.load('lib/libvideo_bbox_drawer.so')

local function read_file(fileName)
    local f = assert(io.open(fileName,'r'))
    local content = f:read('*all')
    f:close()
    return content
end

function _M.draw_bbox(t)
    lib.drawBBoxOnVideo(t.video_file, t.detect_result, #t.detect_result, t.outputfile)
end

return _M