#include "http_upload.h"
#include <iostream>
#include <fstream>      // std::ifstream
#include <sstream>      // std::ifstream

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/beast/core/detail/base64.hpp>

using namespace boost;
using namespace boost::property_tree;
#pragma execution_character_set("utf-8")

void test()
{
// ģ����Ƶ��bbox����
    std::string videoFile = "face_1280_720.h264";
    std::string streamFile = "detect_result.txt";

    std::vector< std::tuple<std::string, std::string> > vec;

    std::ifstream is1(videoFile, std::ifstream::binary);   // open file
    std::ostringstream tmp1;
    tmp1 << is1.rdbuf();

    std::ifstream is2(streamFile, std::ifstream::binary);   // open file
    std::ostringstream tmp2;
    tmp2 << is2.rdbuf();

    vec.push_back(std::make_tuple(videoFile, tmp1.str()));

    std::string host = "192.21.1.43";
    std::string port = "9001";
    std::string url1 = "/upload";

    std::cout << "stream size: " << tmp2.str().size() << std::endl;
    std::string base64_stream = boost::beast::detail::base64_encode(tmp2.str());
    std::cout << "base64_stream size: " << base64_stream.size() << std::endl;
    std::cout << "video upload OK" << std::endl;
    // upload file
    int ret = httpUpload(host, port, url1, vec, base64_stream);
    if (0 == ret)
        std::cout << "video upload OK" << std::endl;
    else
        std::cout << "video upload not OK" << std::endl;
}

int main( int argc, const char** argv )
{
    test();

    return 0;
}
