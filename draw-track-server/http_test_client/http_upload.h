﻿#pragma once
#include <string>
#include <vector>
#include <tuple>

int httpUpload(const std::string &host, const std::string &port, const std::string &url,
         const std::string &filename, const char *data, uint64_t dataSize);

int httpUpload(const std::string &host, const std::string &port, const std::string &url,
         const std::string &filename, const std::string &data);

int httpUpload(const std::string &host, const std::string &port, const std::string &url,
    const std::vector<std::string> &filenames, const std::string &stream = "");

int httpUpload(const std::string &host, const std::string &port, const std::string &url,
         const std::vector<std::tuple<std::string, std::string>> &files, const std::string &stream = "");

int httpPost(const std::string &host, const std::string &port, const std::string &url, const std::string &data);
