## 主要功能

http服务器接收上传的视频和轨迹并调用C库，叠加轨迹到视频中，生成写入新的视频。

## 目录说明

+ http_server：http服务器，接收上传的数据，并调用轨迹叠加C库，使用openresty编写；
+ video_bbox_drawer：轨迹叠加算法库，提供标准C接口，C++编写；
+ http_test_client：测试上传视频和轨迹文件，为C++编写；
+ mock_detect：一个简单的人脸检测程序，模拟算法库，生成轨迹数据并写入文件；[注意mock_detect_def.h要与实际保持一致]。